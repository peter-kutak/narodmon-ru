import psycopg2
from configparser import ConfigParser
from datetime import datetime 

def get_value(name):
  value = None
  conn = None
  try:
    params = config()
    conn = psycopg2.connect(**params)
    cur = conn.cursor()
    cur.execute("SELECT sensors_values.value FROM sensors_values,sensors_sensors WHERE sensors_values.sensor_id = sensors_sensors.id AND sensors_sensors.name='{}' AND sensors_values.time at time zone 'utc'>now()-interval '15 minutes' ORDER BY sensors_values.time DESC LIMIT 1".format(name))
#    print("The number of rows: ", cur.rowcount)
    row = cur.fetchone()
                                                                     
    if row is not None:
      value = row[0]
                         
    cur.close()
    return value
  except (Exception, psycopg2.DatabaseError) as error:
    print(error)
  finally:
    if conn is not None:
      conn.close()

 
def config(filename='database.ini', section='postgresql'):
    # create a parser
    parser = ConfigParser()
    # read config file
    parser.read(filename)
 
    # get section, default to postgresql
    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            db[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))
 
    return db


print("temp: {}".format(get_value("myhome/sensor00aa/temp")))
print("rhum: {}".format(get_value("myhome/sensor00aa/rel.hum")))

