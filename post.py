import psycopg2
from configparser import ConfigParser
from datetime import datetime 

def get_value(name):
  value = None
  conn = None
  row = None
  try:
    params = config()
    conn = psycopg2.connect(**params)
    sensor_id = None
    cur = conn.cursor()
    cur.execute("SELECT id FROM sensors_sensors WHERE name='{}' LIMIT 1".format(name))
    result = cur.fetchone()
    if result is not None:
      sensor_id = result[0]
    cur.close()
    if sensor_id is not None:
      cur = conn.cursor()
      #cur.execute("SELECT value FROM sensors_values WHERE sensor_id = {} AND time at time zone 'utc' > now()-interval '15 minutes' ORDER BY time DESC LIMIT 1".format(sensor_id))
      cur.execute("WITH v AS (SELECT * FROM sensors_values WHERE sensor_id = {} ORDER BY time DESC LIMIT 1) SELECT value FROM v WHERE time at time zone 'utc' > now()-interval '15 minutes'".format(sensor_id))
      #print("The number of rows: ", cur.rowcount)
      row = cur.fetchone()                                                              
      while row is not None:
        #print(row)
        value = row[0]
        row = cur.fetchone()
      cur.close()
    return value
  except (Exception, psycopg2.DatabaseError) as error:
    print(error)
  finally:
    if conn is not None:
      conn.close()

 
def config(filename='database.ini', section='postgresql'):
    # create a parser
    parser = ConfigParser()
    # read config file
    parser.read(filename)
 
    # get section, default to postgresql
    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            db[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))
 
    return db


import requests

def get_radiation_shmu():
    try:
        r = requests.get('http://www.shmu.sk/popups/meteo/radiacia_get_stations_geojson.php')
        for f in r.json()['features']:
            if f['id'] == '11803':
                if f['properties']['prop_state'] == 'blank':
                    return None
                #Trencin
                v = f['properties']['value']
                return int(v)
        return None
    except Exception as e:
        print('Can not get radiation from SHMU')
        print(e)
        return None

def get_radiation():
    v = get_radiation_shmu()
    if v is None: return None
    #nSv/hour -> uR/hour
    v = float(v) / 10
    return v


#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# by Roman Vishnevsky aka.x0x01 @ gmail.com

import urllib3
import urllib
import random

# MAC address of the device. Replace with your own!
DEVICE_MAC = '001018000000'
LAT=49.094500
LNG=18.464400
ELE=340.0

# device ID is added for simplicity, 01 (02) to the mac device
SENSOR_ID_1 = 'T1'
SENSOR_ID_2 = 'H1'
SENSOR_ID_3 = 'R1'
SENSOR_ID_4 = 'MSLP'
SENSOR_ID_5 = 'P1'

# sensor values, float/integer type
sensor_value_1 = get_value("myhome/sensor00aa/temp")
sensor_value_2 = get_value("myhome/sensor00aa/rel.hum")
sensor_value_3 = get_radiation()
sensor_value_4 = get_value("myhome/floor2/largeroom/mslp")
sensor_value_5 = get_value("mppsolar/pv.power")

#100% vlhkost trochu zasumim aby som nemal problem s prisnou kontrolou na narodmon
if sensor_value_2 == 100:
    sensor_value_2 = round(sensor_value_2 - (random.random()/10), 2)
if sensor_value_2 == 20:
    sensor_value_2 = round(sensor_value_2 + (random.random()/10), 2)

if sensor_value_1 is None or sensor_value_2 is None:
    #narodmon mi pri none posiela warning ze ma zablokuje ak neprestanem
    exit()

d = {
    'ID': DEVICE_MAC
    ,'LAT': LAT
    ,'LNG': LNG
    ,'ELE': ELE
    ,SENSOR_ID_1: sensor_value_1
    ,SENSOR_ID_2: sensor_value_2
    }

if sensor_value_3 is not None:
    d[SENSOR_ID_3] = sensor_value_3

if sensor_value_4 is not None:
    d[SENSOR_ID_4] = sensor_value_4

if sensor_value_5 is not None:
    d[SENSOR_ID_5] = sensor_value_5

# formation of POST request for sensors
data = urllib.parse.urlencode(d)

# request header generation
myheaders = {
        'Content-Length': str(len(data)),
        'Content-Type': 'application/x-www-form-urlencoded',
        'Host': 'narodmon.ru'
        }

# directly to a query
http = urllib3.PoolManager()
response = http.request(
        'POST',
        'http://narodmon.ru/post.php', 
        body=data,
        headers=myheaders
        )
print('sent to narodmon.ru {}'.format(response.status))
#print(response.data)
#print(response.headers)

