import psycopg2
from configparser import ConfigParser
from datetime import datetime 

def get_value(name):
  value = None
  conn = None
  try:
    params = config()
    conn = psycopg2.connect(**params)
    cur = conn.cursor()
    cur.execute("SELECT sensors_values.value FROM sensors_values, sensors_sensors WHERE sensors_sensors.id = sensors_values.sensor_id AND sensors_sensors.name='{}' AND sensors_values.time at time zone 'utc' > now()-interval '15 minutes' ORDER BY sensors_values.time DESC LIMIT 1".format(name))
#    print("The number of rows: ", cur.rowcount)
    row = cur.fetchone()
                                                                     
    while row is not None:
#      print(row)
      value = row[0]
      row = cur.fetchone()
                         
    cur.close()
    return value
  except (Exception, psycopg2.DatabaseError) as error:
    print(error)
  finally:
    if conn is not None:
      conn.close()

 
def config(filename='database.ini', section='postgresql'):
    # create a parser
    parser = ConfigParser()
    # read config file
    parser.read(filename)
 
    # get section, default to postgresql
    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            db[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))
 
    return db


# sensor values, float/integer type
sensor_value_1 = get_value("myhome/sensor00aa/temp")


if sensor_value_1 is None :
    exit()

from publish import Publish
p=Publish("outdoors", "kotol", "kotol")
p.publish("outdoors_temp", sensor_value_1)

