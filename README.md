# narodmon-ru

Post data from my database to narodmon.ru service.

## prepare
```
pip install configparser
pip install psycopg2
```

## install
`ln -s /opt/narodmon-ru/narodmon.cron /etc/cron.d/narodmon`